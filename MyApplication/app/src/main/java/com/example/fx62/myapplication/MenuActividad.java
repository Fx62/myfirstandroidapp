package com.example.fx62.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MenuActividad extends Activity implements View.OnClickListener {
    private Button btnBD;
    private Button btnGps;
    private Button btnWs;
    private Button btnPdf;
    private Button btnFirma;
    private Button btnCamara;
    private Button btnSalir;
    private String usuario;
    private TextView lblNombreUsuario;
    private DbData DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_actividad);
        btnBD=(Button) findViewById(R.id.btnDB);
        btnBD.setOnClickListener(this);
        btnGps=(Button) findViewById(R.id.btnGps);
        btnGps.setOnClickListener(this);
        btnWs=(Button) findViewById(R.id.btnWs);
        btnWs.setOnClickListener(this);
        btnPdf=(Button) findViewById(R.id.btnPdf);
        btnPdf.setOnClickListener(this);
        btnFirma=(Button) findViewById(R.id.btnFirma);
        btnFirma.setOnClickListener(this);
        btnCamara=(Button) findViewById(R.id.btnCamara);
        btnCamara.setOnClickListener(this);
        btnSalir=(Button) findViewById(R.id.btnSalir);
        btnSalir.setOnClickListener(this);
        lblNombreUsuario=(TextView) findViewById(R.id.lblNombreUsuario);
        Intent iParametros=getIntent();
        usuario=iParametros.getStringExtra("usuario");
        lblNombreUsuario.setText(usuario);
    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btnDB:
            {
                DB =new DbData(MenuActividad.this);
                DB.test();
                Intent iUsuarios=new Intent(MenuActividad.this,UsuariosActividad.class);
                startActivity(iUsuarios);
                break;
            }
            case R.id.btnSalir:
            {
                finish();
                break;
            }
        }
    }
}
