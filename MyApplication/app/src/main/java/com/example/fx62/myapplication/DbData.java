package com.example.fx62.myapplication;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DbData {
    private static final int DBVER = 1;
    private static final String DBNAME = "dbCurso";

    private DBHelper conn;
    @SuppressWarnings("unused")
    private Context ctx;
    public ModeloUsuario datosUsuario(String codigos){
        SQLiteDatabase db;
        String consulta = "";
        ModeloUsuario registroUsuario = null;
        db = conn.getReadableDatabase();
        consulta = "SELECT a.codigo,a.nombre,a.contrasena ";
        consulta += "FROM usuarios a ";
        consulta = consulta + "WHERE a.codigo = '" + codigos + "'";
        Cursor allrows = db.rawQuery(consulta, null);
        if(allrows.moveToFirst()){
            registroUsuario.setCodigo(allrows.getString(0));
            registroUsuario.setDescripcion(allrows.getString(1));
            registroUsuario.setContrasena(allrows.getString(2));
        }
        db.close();
        return registroUsuario;
    }

    public void modificarUsuario (String codigo, String descripcion, String contrasena){
        SQLiteDatabase db;
        String consulta = "";
        db = conn.getWritableDatabase();
        consulta = "";
        consulta = "UPDATE usuarios ";
        consulta += "SET nombre='" + descripcion + "',contrasena= '" + contrasena + "' WHERE a.codigo" +
                "='" + codigo + "'";
        //db.execSQL(consulta);
        db.close();
    }

    public DbData(Context ctx){
        this.ctx=ctx;
        conn=new DBHelper(ctx);
    }
    public void test()
    {
        SQLiteDatabase db;
        String strSql;
        strSql="";
        strSql=strSql+"SELECT * ";
        strSql=strSql+"FROM usuarios a ";
        db=conn.getReadableDatabase();
        Cursor allrows=db.rawQuery(strSql, null);
        if(allrows.moveToFirst())
        {
            do{

            }while(allrows.moveToNext());
        }
        allrows.close();
        db.close();
    }
    public void insertar_usuario(String CODIGO, String NOMBRE,String CONTRASENA )
    {
        SQLiteDatabase db;
        db = conn.getWritableDatabase();
        String strSql = "";
        strSql = "";
        strSql += "INSERT INTO USUARIOS(CODIGO, NOMBRE,CONTRASENA) ";
        strSql += "VALUES(";
        strSql += "'" + CODIGO + "',";
        strSql += "'" + NOMBRE + "',";
        strSql += "'" + CONTRASENA + "' ) ";
        db.execSQL(strSql);
        db.close();
    }

    public void eliminarUsuario(String codigo){
        SQLiteDatabase db;
        String consulta = "";
        db = conn.getWritableDatabase();
        consulta = "DELETE FROM usuarios WHERE codigo='" + codigo + "'";
        db.execSQL(consulta);
    }



    public ArrayList<ModeloUsuario> listadoUsuarios() {
        SQLiteDatabase db;
        ArrayList<ModeloUsuario> ltemp=new ArrayList<ModeloUsuario>();
        ModeloUsuario RegistroUsuario;
        db=conn.getReadableDatabase();
        String consulta="";
        consulta+="SELECT a.*";
        consulta+="FROM usuarios a";
        Cursor allrows =db.rawQuery(consulta,null);
        if(allrows.moveToFirst())
        {
            do
            {
                RegistroUsuario=new ModeloUsuario();
                RegistroUsuario.setCodigo(allrows.getString(0));
                RegistroUsuario.setDescripcion(allrows.getString(1));
                RegistroUsuario.setContrasena(allrows.getString(2));
                ltemp.add(RegistroUsuario);
            }
            while (allrows.moveToNext());
        }
        return ltemp;
    }

    class DBHelper extends SQLiteOpenHelper {
        public DBHelper(Context context) {
            super(context, DBNAME, null, DBVER);
            // TODO Auto-generated constructor stub
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // TODO Auto-generated method stub
            String strSql = "CREATE TABLE usuarios ("
                    + " codigo TEXT, "
                    + " nombre TEXT,"
                    + " contrasena TEXT) ";
            db.execSQL(strSql);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // TODO Auto-generated method stub
        }
    }
}
