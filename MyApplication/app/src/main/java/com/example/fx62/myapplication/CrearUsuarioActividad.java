package com.example.fx62.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CrearUsuarioActividad extends Activity implements View.OnClickListener {
    private EditText txtCodigoUsuario;
    private EditText txtDescripcionUsuario;
    private EditText txtContrasenaUsuario;
    private Button btnAgregarUsuario;
    private Button btnCancelar;
    private DbData DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crear_usuario_actividad);
        txtCodigoUsuario=(EditText) findViewById(R.id.txtCodigoUsuario);
        txtDescripcionUsuario=(EditText) findViewById(R.id.txtDescripcionUsuario);
        txtContrasenaUsuario=(EditText)findViewById(R.id.txtContrasenaUsuario);
        btnAgregarUsuario=(Button)findViewById(R.id.btnAgregarUsuario);
        btnAgregarUsuario.setOnClickListener(this);
        btnCancelar=(Button)findViewById(R.id.btnCancelar);
        btnCancelar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnAgregarUsuario:
            {
                if(txtCodigoUsuario.getText().toString().equals(""))
                {
                    Toast.makeText(CrearUsuarioActividad.this, "Código de Usuario en Blanco",Toast.LENGTH_SHORT).show();
                    txtCodigoUsuario.requestFocus();
                    return;
                }
                else if (txtDescripcionUsuario.getText().toString().equals(""))
                {
                    Toast.makeText(CrearUsuarioActividad.this, "Nombre de Usuario en Blanco",Toast.LENGTH_SHORT).show();
                    txtDescripcionUsuario.requestFocus();
                    return;
                }
                else if(txtContrasenaUsuario.getText().toString().equals(""))
                {
                    Toast.makeText(CrearUsuarioActividad.this, "Contraseña en Blanco",Toast.LENGTH_SHORT).show();
                    txtContrasenaUsuario.requestFocus();
                    return;
                }
                DB =new DbData(CrearUsuarioActividad.this);
                String codigo=txtCodigoUsuario.getText().toString();
                String nombre=txtDescripcionUsuario.getText().toString();
                String contrasena=txtContrasenaUsuario.getText().toString();
                DB.insertar_usuario(codigo,nombre,contrasena);
                txtCodigoUsuario.setText("");
                txtDescripcionUsuario.setText("");
                txtContrasenaUsuario.setText("");
                Toast.makeText(CrearUsuarioActividad.this, "Usuario Creado con éxito",Toast.LENGTH_SHORT).show();
                txtCodigoUsuario.requestFocus();
                break;
            }
            case R.id.btnCancelar:
            {
                finish();
                break;
            }
        }
    }
}
