package com.example.fx62.myapplication;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CrudUsuariosActividad extends Activity {
    private ListView lsvUsuarios;
    private ArrayList<ModeloUsuario> lUsuarios=new ArrayList<ModeloUsuario>();
    private DbData conData;
    private adaptadorUsuario adpUsuario;
    private String fotrografia = "";
    public static final int CAPTURE_IMAGE = 1777;
    public static final int CAPTURE_SIGNATURE = 1778;
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Acciones");
        menu.add(0, v.getId(), 0, "Modificar Usuario");
        menu.add(0, v.getId(), 0, "Tomar Fotografía");
        menu.add(0, v.getId(), 0, "Firma Usuario");
        menu.add(0, v.getId(), 0, "GPS Método 1");
        menu.add(0, v.getId(), 0, "GPS Método 2");
        menu.add(0, v.getId(), 0, "Firma PDF");
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CAPTURE_IMAGE){
            Toast.makeText(CrudUsuariosActividad.this, "Fotografia grabada correctamente", Toast.LENGTH_SHORT).show();
        }
        if(requestCode == CAPTURE_SIGNATURE){
            Bundle bundle = data.getExtras();
            String status = bundle.getString("status");
            if(status.equalsIgnoreCase("done")){
                Toast toast = Toast.makeText(this, "Firma grabada exitosamente", Toast.LENGTH_SHORT);
                //toast.setGravity(Gravity.TOP, 105, 50);
                toast.show();
            }
        }
    }



    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int intIndexSelected = info.position;
        Object o = lsvUsuarios.getItemAtPosition(intIndexSelected);
        final ModeloUsuario itemUsuario = (ModeloUsuario) o;
        if (item.getTitle().equals("Firma Usuario")){
            Intent intent = new Intent(CrudUsuariosActividad.this, CaptureSignature.class);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, "done");
            startActivityForResult(intent, CAPTURE_SIGNATURE);
            //onActivityResult(1778, 2000, null);
        }
        if (item.getTitle().equals("Modificar Usuario")){
            //Toast.makeText(CrudUsuariosActividad.this, "Presione Modificar Usuario", Toast.LENGTH_LONG).show();
            final Dialog dialog = new Dialog(CrudUsuariosActividad.this);
            dialog.setContentView(R.layout.crear_usuario_actividad);
            dialog.setTitle("Ficha del usuario");
            dialog.setCancelable(false);
            dialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

            EditText txtCodigoUsuario=(EditText) findViewById(R.id.txtCodigoUsuario);
            EditText txtDescripcionUsuario=(EditText) findViewById(R.id.txtDescripcionUsuario);
            EditText txtContrasenaUsuario=(EditText)findViewById(R.id.txtContrasenaUsuario);
            Button btnAgregarUsuario=(Button)findViewById(R.id.btnAgregarUsuario);
            Button btnCancelar=(Button)findViewById(R.id.btnCancelar);

            /*EditText txtCodigoUsuario = (EditText) findViewById(R.id.txtCodigoUsuario);
            EditText txtDescripcionUsuario = (EditText) findViewById(R.id.txtDescripcionUsuario);
            EditText txtContrasenaUsuario = (EditText) findViewById(R.id.txtContrasenaUsuario);
            Button btnAgregarUsuario = (Button) findViewById(R.id.btnAgregarUsuario);
            Button btnCancelar = (Button) findViewById(R.id.btnCancelar);
            txtCodigoUsuario.setText("a", TextView.BufferType.EDITABLE);
            txtDescripcionUsuario.setText("b", TextView.BufferType.EDITABLE);
            txtContrasenaUsuario.setText("c", TextView.BufferType.EDITABLE);*/

            /*txtCodigoUsuario.setEnabled(false);
            btnAgregarUsuario.setText("Modificar Usuario");
            btnAgregarUsuario.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(txtDescripcionUsuario.getText().toString().equals("")){
                        Toast.makeText(CrudUsuariosActividad.this, "Descripcion en blanco", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if(txtContrasenaUsuario.getText().toString().equals("")){
                        Toast.makeText(CrudUsuariosActividad.this, "Contraseña en blanco", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    conData.modificarUsuario(itemUsuario.getCodigo(), txtDescripcionUsuario.getText().toString()
                    , txtContrasenaUsuario.getText().toString());
                    Toast.makeText(CrudUsuariosActividad.this, "usuario actualizado", Toast.LENGTH_SHORT).show();
                    lUsuarios.clear();
                    lUsuarios = conData.listadoUsuarios();
                    adpUsuario = new adaptadorUsuario(CrudUsuariosActividad.this, lUsuarios);
                    lsvUsuarios.setAdapter(adpUsuario);
                    //dialog.dismiss();
                }
            });*/
            dialog.show();
        }
        if (item.getTitle().equals("Tomar Fotografía")){
            //Toast.makeText(CrudUsuariosActividad.this, "Presione Tomar Fotografía", Toast.LENGTH_LONG).show();
            //Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            fotrografia = UUID.randomUUID().toString();
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + fotrografia + ".jpg");
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
            startActivityForResult(intent, CAPTURE_IMAGE);
        }
        if (item.getTitle().equals("GPS Método 1")){
            //Toast.makeText(CrudUsuariosActividad.this, "Presione GPS Método 1", Toast.LENGTH_LONG).show();
            String latitud = "", longitud = "";
            GPSTracker gps = new GPSTracker(CrudUsuariosActividad.this);
            if(gps.canGetLocation){
                latitud = String.valueOf(gps.getLatitude());
                longitud = String.valueOf(gps.getLongitude());
            }
            Toast.makeText(CrudUsuariosActividad.this, "Latitud: " + latitud + ", Longitud: " + longitud, Toast.LENGTH_SHORT).show();
        }
        if (item.getTitle().equals("GPS Método 2")){
            Toast.makeText(CrudUsuariosActividad.this, "Presione GPS Método 2", Toast.LENGTH_LONG).show();
        }
        if (item.getTitle().equals("Ficha PDF")){
            Toast.makeText(CrudUsuariosActividad.this, "Presione Ficha PDF", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crud_usuario_actividad);
        lsvUsuarios=(ListView)findViewById(R.id.lsvUsuarios);
        conData=new DbData(this);
        lUsuarios=conData.listadoUsuarios();
        adpUsuario=new adaptadorUsuario(CrudUsuariosActividad.this,lUsuarios);
        lsvUsuarios.setAdapter(adpUsuario);
        lsvUsuarios.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Object item = lsvUsuarios.getItemAtPosition(position);
                final ModeloUsuario itemUsuario = (ModeloUsuario) item;
                //Toast.makeText(CrudUsuariosActividad.this,
                //        "Nombre" + itemUsuario.getDescripcion(),
                //        Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builder = new AlertDialog.Builder(CrudUsuariosActividad.this);
                builder.setTitle("Accion");
                builder.setMessage("Seguro de eliminar el usuario " + itemUsuario.getDescripcion() + " ?");
                builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        conData.eliminarUsuario(itemUsuario.getCodigo());
                        lsvUsuarios.invalidateViews();
                        lUsuarios.clear();
                        lUsuarios = conData.listadoUsuarios();
                        adpUsuario = new adaptadorUsuario(CrudUsuariosActividad.this, lUsuarios);
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        registerForContextMenu(lsvUsuarios);
    }
}
