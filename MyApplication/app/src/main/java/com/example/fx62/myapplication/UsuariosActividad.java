package com.example.fx62.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class UsuariosActividad extends Activity implements View.OnClickListener{
    private Button btnAgregarNuevoUsuario;
    private Button btnMantenimientoUsuarios;
    private Button btnSalirUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.usuarios_actividad);
        btnAgregarNuevoUsuario=(Button)findViewById(R.id.btnAgregarNuevoUsuario);
        btnAgregarNuevoUsuario.setOnClickListener(this);
        btnMantenimientoUsuarios=(Button)findViewById(R.id.btnMantenimientoUsuarios);
        btnMantenimientoUsuarios.setOnClickListener(this);
        btnSalirUsuario=(Button)findViewById(R.id.btnSalirUsuario);
        btnSalirUsuario.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnAgregarNuevoUsuario:
            {
                Intent iCrearUsuario=new Intent(UsuariosActividad.this, CrearUsuarioActividad.class);
                startActivity(iCrearUsuario);
                break;
            }
            case R.id.btnMantenimientoUsuarios:
            {
                Intent iMantenimiento= new Intent(UsuariosActividad.this,CrudUsuariosActividad.class);
                startActivity(iMantenimiento);
                break;
            }
            case R.id.btnSalirUsuario:
            {
                finish();
                break;
            }
        }
    }
}
