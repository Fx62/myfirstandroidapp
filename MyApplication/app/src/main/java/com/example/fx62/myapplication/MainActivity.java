package com.example.fx62.myapplication;
import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.stetho.Stetho;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity{
    private EditText txtUsuario;
    private EditText txtContrasena;
    private Button btnIngresar;

    public static final int MULTIPLE_PERMISSIONS = 10;

    String[] permissions= new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_FINE_LOCATION,
    };

    private  boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p:permissions) {
            result = ContextCompat.checkSelfPermission(
                    MainActivity.this,p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(
                            new String[listPermissionsNeeded.size()]),
                    MULTIPLE_PERMISSIONS );
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissionsList[], int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS:{
                if (grantResults.length > 0)
                {
                    String permissionsDenied = "";
                    for (String per : permissionsList) {
                        if(grantResults[0] == PackageManager.PERMISSION_DENIED){
                            permissionsDenied += "\n" + per;

                        }

                    }
                    if (!permissionsDenied.equals(""))
                        Toast.makeText(MainActivity.this, "Permisos Denegados: \n "+ permissionsDenied, Toast.LENGTH_SHORT).show();

                }
                return;
            }
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Stetho.initializeWithDefaults(this);
        txtUsuario=(EditText) findViewById(R.id.txtUsuario);
        txtContrasena=(EditText) findViewById(R.id.txtContrasena);
        btnIngresar=(Button)findViewById(R.id.btnIngresar);
        btnIngresar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtUsuario.getText().toString().equals(""))
                {
                    Toast.makeText(MainActivity.this, "Usuario en blanco",Toast.LENGTH_SHORT).show();
                    txtUsuario.requestFocus();
                    return;
                }
                if(txtContrasena.getText().toString().equals(""))
                {
                    Toast.makeText(MainActivity.this, "La Contraseña esta en Blanco", Toast.LENGTH_SHORT).show();
                    txtContrasena.requestFocus();
                    return;
                }
                Intent iMenu=new Intent(MainActivity.this,MenuActividad.class);
                iMenu.putExtra("usuario",txtUsuario.getText().toString());
                startActivity(iMenu);
            }
        });
        if (Build.VERSION.SDK_INT >= 23){
            checkPermissions();
        }
    }
}
