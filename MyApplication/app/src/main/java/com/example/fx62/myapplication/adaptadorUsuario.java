package com.example.fx62.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class adaptadorUsuario extends BaseAdapter {
    private static ArrayList<ModeloUsuario> itemDetailsrrayList;
    private LayoutInflater l_Inflater;
    Context contexto;

    public adaptadorUsuario(Context context, ArrayList<ModeloUsuario> results){
        itemDetailsrrayList=results;
        l_Inflater=LayoutInflater.from(context);
        contexto=context;
    }

    @Override
    public int getCount() {
        return itemDetailsrrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemDetailsrrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder
    {
        TextView linea_cliente_codigo, linea_cliente_descripcion,linea_cliente_contrasena;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView==null)
        {
            convertView=l_Inflater.inflate(R.layout.item,null);
            holder=new ViewHolder();
            holder.linea_cliente_codigo=(TextView) convertView.findViewById(R.id.linea_cliente_codigo);
            holder.linea_cliente_descripcion=(TextView) convertView.findViewById(R.id.linea_cliente_descripcion);
            holder.linea_cliente_contrasena=(TextView) convertView.findViewById(R.id.linea_cliente_contrasena);
            convertView.setTag(holder);
        }
        else
         {
             holder=(ViewHolder) convertView.getTag();
          }
          holder.linea_cliente_codigo.setText(itemDetailsrrayList.get(position).getCodigo());
        holder.linea_cliente_descripcion.setText(itemDetailsrrayList.get(position).getDescripcion());
        holder.linea_cliente_contrasena.setText(itemDetailsrrayList.get(position).getContrasena());
        return convertView;
    }
}
